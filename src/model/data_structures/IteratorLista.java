package model.data_structures;

import java.util.Iterator;

public class IteratorLista <T> implements Iterator<T>
{
	private NodoSencillo<T> actual;
	
	public IteratorLista(NodoSencillo<T> pActual)
	{
		actual=pActual;
	}

	public boolean hasNext() 
	{
		if (actual!=null)
		{
			return true;
		}
		return false;
	}

	public T next() 
	{
		NodoSencillo<T> referencia = actual;
		T item = referencia.darElemento();
		actual = actual.darSiguiente(); 
		return item;
	}

	public void remove() 
	{

	}
}
