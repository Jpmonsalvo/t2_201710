package model.data_structures;

public class NodoSencillo<T> {
	
	private NodoSencillo<T> siguiente;
	private T elemento;
	private int posicion;
	
	public NodoSencillo(T pElemento,int posicion){
		elemento=pElemento;
		siguiente=null;
	}
	public T darElemento(){
		return elemento;
	}
	
	public int darPosicion(){
		return posicion;
	}
	
	public NodoSencillo<T> darSiguiente(){
		return siguiente;
	}
	
	public void modificarSiguiente(NodoSencillo<T> nodo, int pPosicion){
		siguiente=nodo;
		posicion= pPosicion;
	}
	
	public void agregarElemento(T pElemento){
		elemento= pElemento;
	}


}

