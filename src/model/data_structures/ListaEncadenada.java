package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private int tamanio;
	private NodoSencillo<T> primero;
	private NodoSencillo<T> ultimo;
	private NodoSencillo<T> actual;

	public ListaEncadenada()
	{
		tamanio=0;
		primero=null;
		ultimo=null;
		actual=primero;
	}

	public Iterator<T> iterator() 
	{

		return new IteratorLista<T>(primero);
	}

	public void agregarElementoFinal(T elem) {

		Iterator<T> iterador = iterator();

		if(!iterador.hasNext()){
			primero= new NodoSencillo<T>(elem,0);
			ultimo= primero;
			actual=primero;
			tamanio++;
		}
		else
		{
			NodoSencillo<T> elAgregar= new NodoSencillo<T>(elem, ultimo.darPosicion()+1);
			ultimo.modificarSiguiente(elAgregar, elAgregar.darPosicion());
			ultimo=elAgregar;
			tamanio++;
		}
	}

	public T darElemento(int pos) {

		Iterator<T> iterador = iterator();

		while (iterador.hasNext())
		{
			if (actual.darPosicion()==pos)
			{
				return actual.darElemento();
			}
			iterador.next();
		}

		return null;
	}

	public int darNumeroElementos()
	{

		return tamanio;
	}
	
	public T darElementoPosicionActual() 
	{

		return actual.darElemento();
	}

	public boolean avanzarSiguientePosicion() 
	{
		Iterator<T> iterador = iterator();

		if (!iterador.hasNext())
		{
			return false;
		}
		else
		{
			if (actual.darSiguiente()==null )
			{
				return false;
			}
			else
			{
				actual=actual.darSiguiente();
				return true;
			}
		}
	}
	
	public void moverActualInicio()
	{

		actual=primero;		
	}

	public boolean retrocederPosicionAnterior() 
	{

		return false;
	}

}
