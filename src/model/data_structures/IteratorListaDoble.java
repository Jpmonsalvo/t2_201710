package model.data_structures;

import java.util.Iterator;

public class IteratorListaDoble<T> implements Iterator<T>
{
	private NodoDoble<T> actual;
	
	public IteratorListaDoble(NodoDoble<T> pActual)
	{
		actual=pActual;		
	}

	public boolean hasNext()
	{
		if (actual!=null){
			return true;
		}
		return false;
	}
	
	public T next() 
	{
	      NodoDoble<T> referencia = actual;
          T item = referencia.darElemento();
          actual = actual.darSiguiente(); 
          return item;
	}


	public void remove()
	{

	}

}
