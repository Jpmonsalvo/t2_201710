package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

		private int tamanio;
		private NodoDoble<T> primero;
		private NodoDoble<T> ultimo;
		private NodoDoble<T> actual;
		
		public ListaDobleEncadenada()
		{
			tamanio=0;
			primero=null;
			ultimo=null;
			actual=primero;
		}	
	
		public Iterator<T> iterator() 
		{			
		     return new IteratorListaDoble<T>(primero);
		}	
	
		public void agregarElementoFinal(T elem) 
		{			
			Iterator<T> iterador = iterator();
			
			if(!iterador.hasNext())
			{
				primero= new NodoDoble<T>(elem,0);
				ultimo= primero;
				actual=primero;
				tamanio++;
			}
			else
			{			
			NodoDoble<T> elAgregar= new NodoDoble<T>(elem, ultimo.darPosicion()+1);
			ultimo.modificarSiguiente(elAgregar, elAgregar.darPosicion());
			elAgregar.modificarAnterior(ultimo, ultimo.darPosicion());
			ultimo=elAgregar;
			tamanio++;
			}
		}	
	
		public T darElemento(int pos) 
		{
			Iterator<T> iterador = iterator();
			
			while (iterador.hasNext())
			{			  	
				if (actual.darPosicion()==pos)
				{
			  		return actual.darElemento();
			  	}			  	
			  	iterador.next();
			}			
			return null;
		}	
	
		public int darNumeroElementos() 
		{			
			return tamanio;
		}
	

		public T darElementoPosicionActual() 
		{			
			return actual.darElemento();
		}
	

		public boolean avanzarSiguientePosicion() 
		{			
			Iterator<T> iterador = iterator();
			
			if (!iterador.hasNext())
			{
				return false;
			}
			else
			{
				if (actual.darSiguiente()==null )
				{
					return false;
				}
				else
				{
					actual=actual.darSiguiente();
					return true;
				}
			}			
		}
		
		public void moverActualInicio()
		{		
				actual=primero;			
		}
		
		public void moverIzquierda()
		{
			if (primero!=null && actual.darAnterior()!=null && actual!=primero){
				actual=actual.darAnterior();
			}
		}	
		
		public boolean retrocederPosicionAnterior()
		{			
			Iterator<T> iterador = iterator();
			if (!iterador.hasNext())
			{
				return false;
			}
			else
			{
				if (actual.darAnterior()==null || actual.equals(primero))
				{
					return false;
				}
				else
				{
					actual=actual.darAnterior();
					return true;
				}
			}			
		}

}
