package model.logic;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import model.data_structures.ILista;

import model.data_structures.ListaDobleEncadenada;
//
//import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ListaDobleEncadenada<VOPelicula> misPeliculas;
	private ListaDobleEncadenada<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub
		misPeliculas=new ListaDobleEncadenada<>();
		peliculasAgno=new ListaDobleEncadenada<>();
		String csvFile = archivoPeliculas;
		String line = "";
		String cvsSplitBy = ",";

		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
			String sinUso= br.readLine();
			while ((line = br.readLine()) != null) 
			{

				// use comma as separator
				String[] linea = line.split(cvsSplitBy);//0 cod, 1 nombrea�o, 2 genero
				String nombreanio="";
				String generosadiv="";

				if(linea[1].startsWith("\""))
				{
					String[] comacomilla=line.split(","+"\"");
					String[] comillacoma=comacomilla[1].split("\""+",");
					nombreanio=comillacoma[0];
					generosadiv=comillacoma[1];

				}
				else
				{
					nombreanio=linea[1];
					generosadiv=linea[2];
				}

				String nombre;
				String a�o;
				CharSequence cs1 =")";

				if(nombreanio.contains(cs1))
				{
					String[] lineaNA = nombreanio.split("\\(");//0 nombre 1 ... lineaNA.length -1 a�o
					a�o= lineaNA[lineaNA.length-1].substring(0,4);
					String[] nom= nombreanio.split("\\("+ a�o + "\\)");				
					nombre=nom[0].substring(0,nom[0].length()-1);	
				}
				else
				{
					nombre =nombreanio;
					a�o="0";
				}


				String[] lineaG =generosadiv.split("\\|"); 
				ListaDobleEncadenada<String> generos=new ListaDobleEncadenada<String>();
				for(int i =0;i<lineaG.length;i++)
				{
					generos.agregarElementoFinal(lineaG[i]);
				}

				VOPelicula aAgregar = new VOPelicula();
				aAgregar.setTitulo(nombre);
				aAgregar.setAgnoPublicacion(Integer.parseInt(a�o));
				aAgregar.setGenerosAsociados(generos);

				misPeliculas.agregarElementoFinal(aAgregar);
				System.out.println( aAgregar.getAgnoPublicacion()+" " + aAgregar.getTitulo()+" ");


			}
			System.out.println(misPeliculas.darNumeroElementos());

			cargarPelisAnio();



		} catch (IOException e) {
			e.printStackTrace();
		}


	}


	public void cargarPelisAnio()
	{
		peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>();
		for(int i=1950; i <2017; i++ ){
			VOAgnoPelicula elem= new VOAgnoPelicula();
			elem.setAgno(i);
			peliculasAgno.agregarElementoFinal(elem);
		}
		peliculasAgno.moverActualInicio();
		ListaDobleEncadenada<VOPelicula> pelis= new ListaDobleEncadenada<VOPelicula>();
		for(int i=1950; i <= 2017; i ++){
			int a�oD= i;
			VOAgnoPelicula peliActual1= peliculasAgno.darElementoPosicionActual();
			misPeliculas.moverActualInicio();
			for(int j=0; j < misPeliculas.darNumeroElementos(); j++){
				VOPelicula peliActual= misPeliculas.darElementoPosicionActual();
				if(peliActual.getAgnoPublicacion()== peliActual1.getAgno()){ 
					pelis.agregarElementoFinal(peliActual);
				}
				misPeliculas.avanzarSiguientePosicion();
			}
			if(peliculasAgno.darElementoPosicionActual().getAgno()== i){
				peliculasAgno.darElementoPosicionActual().setPeliculas(pelis);
			}

			pelis= new ListaDobleEncadenada<>();
			peliculasAgno.avanzarSiguientePosicion();


		}
	}
	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub

		ILista<VOPelicula> peliculasBuscadas= new ListaDobleEncadenada<VOPelicula>();
		misPeliculas.moverActualInicio();
		for(int i=0; i < misPeliculas.darNumeroElementos(); i++){
			VOPelicula peliActual= misPeliculas.darElementoPosicionActual();
			if(peliActual.getTitulo().contains(busqueda)){
				peliculasBuscadas.agregarElementoFinal(peliActual);
			}
			misPeliculas.avanzarSiguientePosicion();
		}

		return peliculasBuscadas;

	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {


		ILista<VOPelicula> lista= new ListaDobleEncadenada<>();
		misPeliculas.moverActualInicio();
		for (int i=0; i<misPeliculas.darNumeroElementos();i++){
			VOPelicula actual= misPeliculas.darElementoPosicionActual();
			if (actual.getAgnoPublicacion()==agno){
				lista.agregarElementoFinal(actual);

			}
			misPeliculas.avanzarSiguientePosicion();
		}
		regresarPosicion(agno);

		return lista;

	}
	public void regresarPosicion(int n){
		misPeliculas.moverActualInicio();
		for(int i=1950; i<=n;i ++){
			misPeliculas.avanzarSiguientePosicion();
		}
	}
	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub

		VOAgnoPelicula actual=peliculasAgno.darElementoPosicionActual();
		peliculasAgno.avanzarSiguientePosicion();
		actual= peliculasAgno.darElementoPosicionActual();

		return actual;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		peliculasAgno.retrocederPosicionAnterior();

		return peliculasAgno.darElementoPosicionActual();

	}

}
