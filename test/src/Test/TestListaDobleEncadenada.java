package Test;

import junit.framework.TestCase;
import model.data_structures.ListaDobleEncadenada;

public class TestListaDobleEncadenada<T> extends TestCase{

	ListaDobleEncadenada<String> lista;
	ListaDobleEncadenada<Double> lista2;

	public  void setupEscenario1(){
		lista= new ListaDobleEncadenada<String>();
	}
	public void setupEscenario2(){
		lista2=new ListaDobleEncadenada<Double>();
	}


	public void testAgregarAlFinalyDarElem(){
		setupEscenario1();

		String x= "x";
		String y= "y";
		String z = "z";

		lista.agregarElementoFinal(x);
		lista.agregarElementoFinal(y);
		lista.agregarElementoFinal(z);
		assertEquals("El elemento no es el esperado", x,lista.darElemento(0));
		assertEquals("El elemento no es el esperado", y,lista.darElemento(1));
		assertEquals("El elemento no es el esperado", z,lista.darElemento(2));
	}

	public void testDarElementoPosActual(){
		String x= "x";
		lista.agregarElementoFinal(x);
		assertEquals("El elemento no es el esperado", x,lista.darElementoPosicionActual());
	}
	public void testDarNumeroElementos(){
		String x= "x";
		String y= "y";
		String z = "z";

		lista.agregarElementoFinal(x);
		lista.agregarElementoFinal(y);
		lista.agregarElementoFinal(z);

		assertEquals("El numero de elementos no es el esperado", 3,lista.darNumeroElementos());

	}
	public void testAvanzarSigPos(){
		String x= "x";
		String y= "y";
		String z = "z";

		lista.agregarElementoFinal(x);
		lista.agregarElementoFinal(y);
		lista.agregarElementoFinal(z);
		lista.avanzarSiguientePosicion();
		assertEquals("El elemento no es el esperado", y,lista.darElementoPosicionActual());
	}
	public void testRetrocederPosAnt(){
		String x= "x";
		String y= "y";
		String z = "z";

		lista.agregarElementoFinal(x);
		lista.agregarElementoFinal(y);
		lista.agregarElementoFinal(z);
		lista.avanzarSiguientePosicion();
		lista.avanzarSiguientePosicion();
		lista.retrocederPosicionAnterior();
		assertEquals("El elemento no es el esperado", y,lista.darElementoPosicionActual());
	}

}
